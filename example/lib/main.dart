import 'dart:io';

import 'package:dub_image_picker/dub_image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Dub Image Picker'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<File> files = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
              onPressed: () {
                setState(() {
                  files = [];
                });
              },
              icon: const Icon(Icons.delete))
        ],
      ),
      body: CustomScrollView(
        slivers: [
          SliverList(
            delegate: SliverChildListDelegate(files.map((e) => Image.file(e)).toList()),
          )
        ],
      ),
      bottomNavigationBar: Container(
        height: 56 + MediaQuery.of(context).padding.bottom,
        child: SafeArea(
          child: BottomAppBar(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                    child: Center(
                        child: CupertinoButton(
                            minSize: 0,
                            onPressed: () async {
                              List<File>? image = await DubImagePicker.getImages(context,
                                  maxCount: 5,
                                  primaryColor: Colors.red,
                                  title: Text('hello owrld',
                                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                  countBuilder: (cur, int) {
                                return Container(
                                    height: 44,
                                    color: Colors.white,
                                    child: Row(
                                      children: [
                                        Text('${cur} : ${int}'),
                                      ],
                                    ));
                              }, actionBuilder: (action) {
                                return OutlinedButton(
                                  onPressed: () {
                                    action.call();
                                  },
                                  child: SizedBox(),
                                );
                              }, emptyWidget: Text('hello widget', style: TextStyle(fontWeight: FontWeight.bold)));
                              if (image != null && image.isNotEmpty) {
                                setState(() {
                                  files = image;
                                  print('file size : ${files[0].lengthSync()}');
                                });
                              }
                            },
                            child: Text('pick 5')))),
                Expanded(
                    child: Center(
                        child: CupertinoButton(
                            minSize: 0,
                            onPressed: () async {
                              File? image = await DubImagePicker.getImage(context,
                                  title: Text('hello owrld',
                                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)));
                              if (image != null) {
                                setState(() {
                                  files = [image];
                                  print('file size : ${image.lengthSync()}');
                                });
                              }
                            },
                            child: Text('pick 1')))),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
