library dub_image_picker;

import 'dart:io';

import 'package:dub_image_picker/src/util/page_route.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

import 'src/exception/permissionException.dart';
import 'src/screens/image_picker_view.dart';

class DubImagePicker {
  DubImagePicker();

  static Future<File?> getImage(
    BuildContext context, {
    Color? primaryColor,
    Widget? title,
    Widget? emptyWidget,
  }) async {
    bool hasPermission = false;

    hasPermission = await checkUserPermission(permission: Permission.photos);
    if (!hasPermission) {
      hasPermission = await checkUserPermission(permission: Permission.storage);
    }
    if (hasPermission) {
      File? result = await Navigator.of(context).push(PickerPageRoute(
          fullscreenDialog: true,
          builder: (ctx) => ImagePickerView(
                multi: false,
                primaryColor: primaryColor,
                title: title,
                emptyWidget: emptyWidget,
              )));
      return result;
    } else {
      throw PermissionException('permission not granted');
    }
  }

  static Future<List<File>?> getImages(
    BuildContext context, {
    int? maxCount = 3,
    Color? primaryColor,
    Widget? title,
    CounterBuilder? countBuilder,
    ActionBuilder? actionBuilder,
    int? currentCount = 0,
    Widget? emptyWidget,
  }) async {
    bool hasPermission = false;
    hasPermission = await checkUserPermission(permission: Permission.photos);
    if (!hasPermission) {
      hasPermission = await checkUserPermission(permission: Permission.storage);
    }
    if (hasPermission) {
      List<File>? result = await Navigator.of(context).push(PickerPageRoute(
          fullscreenDialog: true,
          builder: (ctx) => ImagePickerView(
                multi: true,
                maxCount: maxCount,
                currentCount: currentCount,
                primaryColor: primaryColor,
                title: title,
                counter: countBuilder,
                onAction: actionBuilder,
                emptyWidget: emptyWidget,
              )));
      return result;
    } else {
      throw PermissionException('permission not granted');
    }
  }

  static Future<bool> checkUserPermission({required Permission permission}) async {
    await permission.request();

    var permissionStatus = await permission.status;
    bool hasPermission = false;

    if (permissionStatus.isGranted || permissionStatus.isLimited) {
      hasPermission = true;
    } else {
      hasPermission = false;
    }

    return hasPermission;
  }
}
