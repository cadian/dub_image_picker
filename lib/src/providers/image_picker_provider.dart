// ignore_for_file: file_names

import 'dart:io';

import 'package:dub_image_picker/src/models/media.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:rxdart/rxdart.dart';

const _defaultLimit = 120;

class ImagePickerProvider extends ChangeNotifier {
  final int? maxCount;
  final int? currentcount;
  final PublishSubject<bool> onProgress = PublishSubject();
  List<Media>? mediaList;
  List<AssetEntity>? selectedMediaList;
  AssetPathEntity? album;
  int page = 0;
  bool isFinished = false;

  select(index) {
    if ((selectedMediaList?.length ?? 0) + (currentcount ?? 0) >= (maxCount ?? 3)) {
      return;
    }
    Media? selectedElement = mediaList?.elementAt(index);

    if (selectedElement == null) {
      return;
    }

    selectedElement.selected = true;
    AssetEntity medium = selectedElement.medium;
    if (!(selectedMediaList?.contains(medium) ?? false)) {
      selectedMediaList = [...selectedMediaList ?? [], medium];
    }
    notifyListeners();
  }

  bool isSelected(index) {
    return selectedMediaList?.contains(mediaList!.elementAt(index).medium) ?? false;
  }

  int getSelectedIndex(index) {
    return selectedMediaList?.indexOf(mediaList!.elementAt(index).medium) ?? 0;
  }

  unSelect(index) {
    mediaList?.elementAt(index).selected = false;
    AssetEntity medium = mediaList!.elementAt(index).medium;
    selectedMediaList?.remove(medium);
    notifyListeners();
  }

  Future<void> getMediaList() async {
    try {
      if (isFinished) {
        return;
      }
      AssetPathEntity? pageAlbum = album;
      if (pageAlbum == null) {
        return;
      }
      List<AssetEntity> entries = await pageAlbum.getAssetListPaged(page: page++, size: _defaultLimit);
      List<Media> temp = entries.map((e) => Media(e)).toList();
      mediaList ??= [];
      if (temp.isEmpty) {
        isFinished = true;
        notifyListeners();
        return;
      }
      mediaList?.addAll(temp);
      notifyListeners();
    } catch (e) {}
  }

  Future<void> onLoading() async {
    await getMediaList();
  }

  Future<void> onRefresh() async {
    mediaList = selectedMediaList = album = null;
    page = 0;
    isFinished = false;
    notifyListeners();
    await getAlbums();
  }

  @override
  void dispose() {
    PaintingBinding.instance?.imageCache?.clear();
    PaintingBinding.instance?.imageCache?.clearLiveImages();
    onProgress.close();
    super.dispose();
  }

  ImagePickerProvider({this.maxCount, this.currentcount}) {
    // Future.delayed(const Duration(milliseconds: 600), () {
    getAlbums();
    // });
  }

  Future<void> getAlbums() async {
    List<AssetPathEntity> _imageAlbums = await PhotoManager.getAssetPathList(onlyAll: true, type: RequestType.image);
    _imageAlbums = _imageAlbums.where((e) => e.isAll).toList();
    if (_imageAlbums.isNotEmpty) {
      album = _imageAlbums.first;
      await getMediaList();
    } else {
      mediaList = [];
      notifyListeners();
    }
  }

  Future<List<File>> convertToFileList(List<AssetEntity>? entries) async {
    try {
      setLoading(true);
      Directory tempDir = await getTemporaryDirectory();
      List<File?> files = await Future.wait<File?>(entries?.map<Future<File?>>((e) {
            return e.file;
          }).toList() ??
          []);

      List<File?> compressedFiles = await Future.wait<File?>(files.map<Future<File?>>((file) {
        String filename = basenameWithoutExtension(file!.path);
        String targetPath = tempDir.absolute.path + "/$filename" + '.jpg';

        if (File(targetPath).existsSync()) {
          File(targetPath).deleteSync();
        }

        var compressFile = FlutterImageCompress.compressAndGetFile(file.absolute.path, targetPath,
            minHeight: 1200, minWidth: 1200, quality: 50, format: CompressFormat.jpeg);
        return compressFile;
      }));

      List<File>? filesWithoutNull = [];

      for (File? file in compressedFiles) {
        if (file != null) {
          filesWithoutNull.add(file);
        }
      }

      setLoading(false);
      return filesWithoutNull;
    } catch (e, stack) {
      debugPrint('error in compress = $e : $stack');
      return [];
    }
  }

  Future<File?> convertToFile(AssetEntity? entry) async {
    try {
      setLoading(true);
      Directory tempDir = await getTemporaryDirectory();

      File? file = await entry?.file;

      String filename = basenameWithoutExtension(file!.path);
      String targetPath = tempDir.absolute.path + "/$filename" + '.jpg';

      if (File(targetPath).existsSync()) {
        File(targetPath).deleteSync();
      }

      File? compressedFile = await FlutterImageCompress.compressAndGetFile(
        file.absolute.path,
        targetPath,
        minHeight: 1200,
        minWidth: 1200,
        quality: 50,
      );

      setLoading(false);
      return compressedFile;
    } catch (e) {
      setLoading(false);
      return null;
    }
  }

  setLoading(bool isLoading) {
    if (onProgress.isClosed) {
      return;
    }
    onProgress.add(true);
  }
}
