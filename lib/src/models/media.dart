import 'package:photo_manager/photo_manager.dart';

class Media {
  bool? selected;
  AssetEntity medium;

  Media(this.medium) {
    selected = false;
  }
}
