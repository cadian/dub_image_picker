import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart' as rx;

enum LoadingStatus { LOADING, STABLE, FINISHED }

/// A widget that wraps a [Widget] and will trigger [onEndOfPage] when it
/// reaches the bottom of the list
///
///

class LazyLoadScrollView extends StatelessWidget {
  final Widget child;
  final Function onEndOfPage;
  final int scrollOffset;
  final bool isFinished;
  final Function? onLoadingCallBack;
  final bool hasIndicator;
  final EdgeInsets indicatorPadding;
  final ValueChanged<double>? onVerticalDrag;
  final ScrollPhysics? physics;

  final rx.BehaviorSubject<bool> isLoading = rx.BehaviorSubject<bool>.seeded(false);
  LoadingStatus loadMoreStatus = LoadingStatus.STABLE;
  int checkInterval = 0;
  late CustomScrollView scrollView;

  LazyLoadScrollView({
    Key? key,
    required this.child,
    required this.onEndOfPage,
    this.onVerticalDrag,
    this.isFinished = false,
    this.scrollOffset = 200,
    this.hasIndicator = false,
    this.indicatorPadding = const EdgeInsets.all(0.0),
    this.onLoadingCallBack,
    this.physics,
  })
      : assert(onEndOfPage != null),
        assert(child is CustomScrollView),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    CustomScrollView scrollView = child as CustomScrollView;
    scrollView = CustomScrollView(
      physics: physics ?? CustomBouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      controller: scrollView.controller,
      key: scrollView.key,
      slivers: scrollView.slivers,
      clipBehavior: scrollView.clipBehavior,
      dragStartBehavior: scrollView.dragStartBehavior,
      primary: scrollView.primary,
      scrollDirection: scrollView.scrollDirection,
      anchor: scrollView.anchor,
      cacheExtent: scrollView.cacheExtent,
      restorationId: scrollView.restorationId,
      reverse: scrollView.reverse,
      semanticChildCount: scrollView.semanticChildCount,
      shrinkWrap: scrollView.shrinkWrap,
    );

    if (!scrollView.slivers.contains(LoadIndicator)) {
      if (hasIndicator) {
        scrollView.slivers.add(LoadIndicator(isLoading: isLoading, padding: indicatorPadding,));
      }
    }

    return StreamDelegateBuilder(
        isLoading: isLoading,
        builder: (ctx) {
          return NotificationListener(
            child: scrollView,
            onNotification: (dynamic notification) {
              checkInterval++;
              if (checkInterval % 5 == 0) {
                _onNotification(notification, context, isLoading);
                checkInterval = 0;
              }
              return false;
            },
          );
        });
  }

  addLoadingStream(bool value, rx.BehaviorSubject<bool> isLoading) {
    if (isLoading.isClosed == false) {
      isLoading.add(value);
    }
  }

  _onNotification(Notification notification, BuildContext context, rx.BehaviorSubject<bool> isLoading) async {
    if (loadMoreStatus == LoadingStatus.LOADING || isFinished == true) {
      if (onVerticalDrag != null) {
        onVerticalDrag!(0.0);
      }
      return true;
    }

    if (isLoading.value) {
      return true;
    }
    if (notification is ScrollUpdateNotification) {
      if (notification.metrics.axis == Axis.horizontal) {
        return;
      }
      if (onVerticalDrag != null) {
        onVerticalDrag!(notification.metrics.pixels);
      }
      if (notification.metrics.maxScrollExtent <= notification.metrics.pixels + scrollOffset) {
        if (loadMoreStatus == LoadingStatus.STABLE) {
          loadMoreStatus = LoadingStatus.LOADING;
          if (onLoadingCallBack != null) {
            onLoadingCallBack!(true);
          }

          addLoadingStream(true, isLoading);
          await onEndOfPage();
          addLoadingStream(false, isLoading);
          loadMoreStatus = LoadingStatus.STABLE;
          if (onLoadingCallBack != null) {
            onLoadingCallBack!(false);
          }
          return true;
        }
      }

      return;
    }

    if (notification is OverscrollNotification) {
      if (notification.metrics.axis == Axis.horizontal) {
        return;
      }
      if (notification.overscroll > 0) {
        if (loadMoreStatus != null && loadMoreStatus == LoadingStatus.STABLE) {
          loadMoreStatus = LoadingStatus.LOADING;
          addLoadingStream(true, isLoading);
          await onEndOfPage();
          addLoadingStream(false, isLoading);
          loadMoreStatus = LoadingStatus.STABLE;
        }
      }
      return;
    }
    return;
  }
}

class LoadIndicator extends StatelessWidget {
  const LoadIndicator({
    Key? key,
    required this.isLoading,
    this.padding = const EdgeInsets.all(0.0),
  }) : super(key: key);

  final EdgeInsets padding;
  final rx.BehaviorSubject<bool> isLoading;

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
        child: StreamBuilder<bool>(
            stream: isLoading,
            initialData: false,
            builder: (context, snapshot) {
              if (snapshot.data!) {
                return Padding(
                  padding: padding,
                  child: const SafeArea(
                      top: false,
                      minimum: EdgeInsets.only(bottom: 16, top: 16),
                      child: Center(child: CircularProgressIndicator())),
                );
              } else {
                return const SizedBox();
              }
            }));
  }
}

class CustomBouncingScrollPhysics extends ScrollPhysics {
  const CustomBouncingScrollPhysics({ScrollPhysics? parent}) : super(parent: parent);

  @override
  CustomBouncingScrollPhysics applyTo(ScrollPhysics? ancestor) {
    return CustomBouncingScrollPhysics(parent: buildParent(ancestor));
  }

  double frictionFactor(double overscrollFraction) => 0.52 * pow(1 - overscrollFraction, 2);

  @override
  double applyPhysicsToUserOffset(ScrollMetrics position, double offset) {
    assert(offset != 0.0);
    assert(position.minScrollExtent <= position.maxScrollExtent);

    if (!position.outOfRange) return offset;

    final double overscrollPastStart = max(position.minScrollExtent - position.pixels, 0.0);
    final double overscrollPastEnd = max(position.pixels - position.maxScrollExtent, 0.0);
    final double overscrollPast = max(overscrollPastStart, overscrollPastEnd);
    final bool easing = (overscrollPastStart > 0.0 && offset < 0.0) || (overscrollPastEnd > 0.0 && offset > 0.0);

    final double friction = easing
        ? frictionFactor((overscrollPast - offset.abs()) / position.viewportDimension)
        : frictionFactor(overscrollPast / position.viewportDimension);
    final double direction = offset.sign;

    return direction * _applyFriction(overscrollPast, offset.abs(), friction);
  }

  static double _applyFriction(double extentOutside, double absDelta, double gamma) {
    assert(absDelta > 0);
    double total = 0.0;
    if (extentOutside > 0) {
      final double deltaToLimit = extentOutside / gamma;
      if (absDelta < deltaToLimit) return absDelta * gamma;
      total += extentOutside;
      absDelta -= deltaToLimit;
    }
    return total + absDelta;
  }

  @override
  double applyBoundaryConditions(ScrollMetrics position, double value) {
    return 0.0;
  }

  @override
  Simulation? createBallisticSimulation(ScrollMetrics position, double velocity) {
    final Tolerance tolerance = this.tolerance;

    double velocityCopied = velocity;
    if (velocity.isNegative) {
      velocityCopied = max(velocity, -6000);
    } else {
      velocityCopied = min(velocity, 6000);
    }
    if (velocity.abs() >= tolerance.velocity || position.outOfRange) {
      return BouncingScrollSimulation(
        spring: spring,
        position: position.pixels,
        velocity: velocityCopied,
        leadingExtent: position.minScrollExtent,
        trailingExtent: position.maxScrollExtent,
        tolerance: tolerance,
      );
    }

    return null;
  }

  @override
  double get minFlingVelocity => kMinFlingVelocity * 2.0;

  @override
  double carriedMomentum(double existingVelocity) {
    return existingVelocity.sign * min(0.000816 * pow(existingVelocity.abs(), 1.967).toDouble(), 40000.0);
  }

  @override
  bool recommendDeferredLoading(double velocity, ScrollMetrics metrics, BuildContext context) {
    return super.recommendDeferredLoading(velocity, metrics, context);
  }

  @override
  double adjustPositionForNewDimensions({required ScrollMetrics oldPosition,
    required ScrollMetrics newPosition,
    required bool isScrolling,
    required double velocity}) {
    if (oldPosition.extentAfter == 0 && newPosition.extentAfter != 0) {
      if (oldPosition.extentInside != newPosition.extentInside) {
        return super.adjustPositionForNewDimensions(
            oldPosition: oldPosition,
            newPosition: FixedScrollMetrics(
              viewportDimension: oldPosition.viewportDimension,
              pixels: newPosition.extentBefore,
              minScrollExtent: oldPosition.minScrollExtent,
              maxScrollExtent: oldPosition.maxScrollExtent,
              axisDirection: oldPosition.axisDirection,
            ),
            isScrolling: isScrolling,
            velocity: velocity);
      }
    }
    return super.adjustPositionForNewDimensions(
        oldPosition: oldPosition, newPosition: newPosition, isScrolling: isScrolling, velocity: velocity);
  }

  @override
  double get dragStartDistanceMotionThreshold => 0.5;
}

typedef rxBuilder = Widget Function(BuildContext context);

class StreamDelegateBuilder extends StatefulWidget {
  /// Creates a widget that both has state and delegates its build to a callback.
  ///
  /// The [builder] argument must not be null.
  const StreamDelegateBuilder({
    Key? key,
    required this.builder,
    this.isLoading,
  })
      : assert(builder != null),
        super(key: key);

  /// Called to obtain the child widget.
  ///
  /// This function is called whenever this widget is included in its parent's
  /// build and the old widget (if any) that it synchronizes with has a distinct
  /// object identity. Typically the parent's build method will construct
  /// a new tree of widgets and so a new Builder child will not be [identical]
  /// to the corresponding old one.
  final rxBuilder builder;
  final rx.BehaviorSubject<bool>? isLoading;

  @override
  _StreamDelegateBuilderState createState() => _StreamDelegateBuilderState();
}

class _StreamDelegateBuilderState extends State<StreamDelegateBuilder> {
  @override
  Widget build(BuildContext context) => widget.builder(context);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    widget.isLoading!.close();
    super.dispose();
  }
}
