import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';

class AssetImageWidget extends StatefulWidget {
  final AssetEntity? assetEntity;
  final double? width;
  final double? height;

  const AssetImageWidget({
    Key? key,
    required this.assetEntity,
    this.width = 300,
    this.height = 300,
  }) : super(key: key);

  @override
  State<AssetImageWidget> createState() => _AssetImageWidgetState();
}

class _AssetImageWidgetState extends State<AssetImageWidget> {
  late Future<Uint8List?>? task;
  @override
  void initState() {
    task = widget.assetEntity
        ?.thumbnailDataWithOption(ThumbnailOption(quality: 50, size: ThumbnailSize.square(widget.width!.toInt())));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.assetEntity == null) {
      return SizedBox();
    }

    return Container(
      color: Colors.grey[100],
      child: FutureBuilder<Uint8List?>(
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            Uint8List? uint8List = snapshot.data;
            return Image.memory(
              uint8List!,
              width: widget.width,
              height: widget.height,
              fit: BoxFit.cover,
              cacheWidth: widget.width!.toInt(),
              errorBuilder: (ctx, _, __) {
                return SizedBox();
              },
            );
          } else {
            return SizedBox(
              height: widget.height,
              width: widget.width,
            );
          }
        },
        future: task,
      ),
    );
  }
}
