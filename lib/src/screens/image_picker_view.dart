// ignore_for_file: file_names

import 'dart:io';

import 'package:dub_image_picker/src/providers/image_picker_provider.dart';
import 'package:dub_image_picker/src/widgets/asset_image_widget.dart';
import 'package:dub_image_picker/src/widgets/lazy_loading_scrollview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const _defaultColor = Colors.red;

typedef CounterBuilder = Widget Function(int current, int max);
typedef ActionBuilder = Widget Function(Function onSubmit);

class ImagePickerView extends StatefulWidget {
  final Color? primaryColor;
  final bool? multi;
  final int? maxCount;
  final int? currentCount;
  final CounterBuilder? counter;
  final ActionBuilder? onAction;
  final Widget? title;
  final Widget? emptyWidget;

  const ImagePickerView({
    Key? key,
    this.multi,
    this.maxCount = 3,
    this.currentCount = 0,
    this.primaryColor,
    this.counter,
    this.onAction,
    this.title,
    this.emptyWidget,
  }) : super(key: key);

  @override
  State<ImagePickerView> createState() => _ImagePickerViewState();
}

class _ImagePickerViewState extends State<ImagePickerView> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ImagePickerProvider(maxCount: widget.maxCount, currentcount: widget.currentCount),
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.white,
            appBar: PreferredSize(
              preferredSize: Size(0, 44),
              child: AppBar(
                elevation: 0,
                automaticallyImplyLeading: false,
                backgroundColor: Colors.white,
                centerTitle: true,
                title: widget.title ?? SizedBox(),
                leading: IconButton(
                    icon: const Icon(CupertinoIcons.clear_thick, color: Colors.black),
                    onPressed: () {
                      Navigator.of(context).pop(null);
                    }),
                actions: [
                  Consumer<ImagePickerProvider>(
                    builder: (ctx, prov, child) => Builder(
                      builder: (ctx) {
                        if (widget.onAction != null) {
                          return widget.onAction!(() async {
                            List<File> files = await prov.convertToFileList(prov.selectedMediaList);
                            if (mounted) {
                              Navigator.pop(context, files);
                            }
                          });
                        } else {
                          return SizedBox(
                            width: 1,
                            height: 1,
                          );
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
            body: Column(
              children: [
                Consumer<ImagePickerProvider>(
                  builder: (ctx, prov, child) => Builder(
                    builder: (ctx) {
                      if (widget.counter != null) {
                        return widget.counter!(
                            (prov.selectedMediaList?.length ?? 0) + (widget.currentCount ?? 0), widget.maxCount ?? 0);
                      } else {
                        return SizedBox();
                      }
                    },
                  ),
                ),
                Expanded(
                  child: Consumer<ImagePickerProvider>(
                    builder: (context, prov, child) {
                      return LazyLoadScrollView(
                        scrollOffset: 3000,
                        onEndOfPage: () async {
                          await prov.onLoading();
                        },
                        child: CustomScrollView(
                          cacheExtent: MediaQuery.of(context).size.height * 3,
                          slivers: [
                            ImagePickerList(
                              isMulti: widget.multi,
                              primaryColor: widget.primaryColor ?? _defaultColor,
                              emptyWidget: widget.emptyWidget,
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          buildProgressView(),
        ],
      ),
    );
  }

  Widget buildProgressView() {
    return Consumer<ImagePickerProvider>(
        builder: (ctx, prov, child) {
          return StreamBuilder<bool>(
              stream: prov.onProgress,
              initialData: false,
              builder: (context, snapshot) {
                bool isOnProgress = snapshot.data as bool;

                if (isOnProgress) {
                  return child!;
                } else {
                  return SizedBox();
                }
              });
        },
        child: Positioned.fill(
            child: Container(
                decoration: BoxDecoration(color: Colors.black.withOpacity(.5)), child: Center(child: _Indicator()))));
  }
}

class ImagePickerList extends StatelessWidget {
  const ImagePickerList({Key? key, this.isMulti = false, required this.primaryColor, this.emptyWidget})
      : super(key: key);
  final Color primaryColor;
  final bool? isMulti;
  final Widget? emptyWidget;

  @override
  Widget build(BuildContext context) {
    ImagePickerProvider prov = Provider.of<ImagePickerProvider>(context, listen: false);

    if (prov.mediaList == null) {
      return const SliverFillRemaining(
        child: Align(alignment: Alignment(0.0, -0.2), child: _Indicator()),
      );
    }

    if (prov.mediaList!.isEmpty) {
      return SliverFillRemaining(
        child: Align(alignment: const Alignment(0.0, -0.2), child: emptyWidget ?? SizedBox()),
      );
    }

    return SliverGrid(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        childAspectRatio: 1,
        mainAxisSpacing: 2,
        crossAxisSpacing: 2,
      ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          var assetEntity = prov.mediaList!.elementAt(index).medium;

          return GestureDetector(
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                Positioned.fill(
                  child: AssetImageWidget(
                    assetEntity: assetEntity,
                  ),
                ),
                Positioned.fill(child: Builder(builder: (context) {
                  bool isSelcted = prov.isSelected(index);
                  return Stack(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                            color: isSelcted ? Colors.black.withOpacity(.4) : null,
                            border: isSelcted ? Border.all(width: 2, color: primaryColor) : Border()),
                      ),
                      //BADGE
                      Positioned(
                          top: 4,
                          right: 4,
                          child: Builder(
                            builder: (ctx) {
                              if (isMulti ?? false) {
                                if (isSelcted) {
                                  return _SelectedBadge(
                                    number: prov.getSelectedIndex(index) + 1,
                                    primaryColor: primaryColor,
                                  );
                                } else {
                                  return _UnSelectedBadge();
                                }
                              } else {
                                return SizedBox();
                              }
                            },
                          )),
                    ],
                  );
                })),
              ],
            ),
            onTap: () async {
              if (isMulti ?? false) {
                if (prov.isSelected(index)) {
                  prov.unSelect(index);
                } else {
                  prov.select(index);
                }
              } else {
                File? file = await prov.convertToFile(assetEntity);
                Navigator.of(context).pop(file);
              }
            },
          );
        },
        childCount: prov.mediaList?.length ?? 0,
      ),
    );
  }
}

class _SelectedBadge extends StatelessWidget {
  const _SelectedBadge({
    Key? key,
    required this.number,
    required this.primaryColor,
  }) : super(key: key);

  final int number;
  final Color primaryColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 18,
      height: 18,
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: Colors.white),
        color: primaryColor,
        shape: BoxShape.circle,
      ),
      child: Center(
        child: FittedBox(
          child: Text(
            '$number',
            style: const TextStyle(color: Colors.white, fontSize: 10, height: 1),
          ),
        ),
      ),
    );
  }
}

class _UnSelectedBadge extends StatelessWidget {
  const _UnSelectedBadge({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 18,
        height: 18,
        decoration: BoxDecoration(
            border: Border.all(width: 1, color: Colors.white),
            color: Colors.white.withOpacity(.5),
            shape: BoxShape.circle));
  }
}

class _Indicator extends StatelessWidget {
  const _Indicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(cupertinoOverrideTheme: CupertinoThemeData(brightness: Brightness.light)),
        child: CupertinoActivityIndicator(
          radius: 15,
        ));
  }
}
