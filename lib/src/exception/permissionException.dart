class PermissionException implements Exception {
  final dynamic message;

  PermissionException([this.message]);

  String toString() {
    Object? message = this.message;
    if (message == null) return "PermissionExcetion";
    return "PermissionExcetion: $message";
  }
}
